const { createApp } = Vue;

createApp({
  data() {
    return {
      downloadList: [
        {
          link: 'https://public-assests.s3.us-east-2.amazonaws.com/kusalassaUpasampada/Budda+Jayanthi+Damma+Vinaya+213+mb.zip',
          name: 'Budda Jayanthi Damma and Vinay in pali)',
        },
        {
          link: 'https://public-assests.s3.us-east-2.amazonaws.com/kusalassaUpasampada/english+translation+of+the+suttas.zip',
          name: 'English translation of the Suttas',
        },
        {
          link: 'https://public-assests.s3.us-east-2.amazonaws.com/kusalassaUpasampada/Kusalassa+Upasamp%C4%81da.zip',
          name: 'Kusalassa Upasampāda',
        },
        {
          link: 'https://public-assests.s3.us-east-2.amazonaws.com/kusalassaUpasampada/the+suttas+and+vinaya+in+roman+letters.zip',
          name: 'The Suttas and Vinaya in roman letters',
        },
        {
          link: 'https://public-assests.s3.us-east-2.amazonaws.com/kusalassaUpasampada/the+suttas+translated+by+mahamjuunawa.zip',
          name: 'The Suttas translated by Mahamjuunawa',
        },
        {
          link: 'https://public-assests.s3.us-east-2.amazonaws.com/kusalassaUpasampada/Buddhajayanthi+Sutta+and+Vinaya+Sinhala+and+pali.zip',
          name: 'Budda Jayanthi Sutta and Vinaya in sinhala and pali',
        },
      ],
    };
  },
}).mount('#vueContainer');
